day08 07-07

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 完成客服聊天功能 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/myflask
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 富文本编辑器+评论情感分析 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 码云，钉钉，微博 第三方登录套娃模式 | 注意回调地址 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 课程评论功能 | 暂时没有 | https://gitee.com/wang_xiao_rui/mydjango_dev.git    
https://gitee.com/wang_xiao_rui/myvue_dev.git
https://wang_xiao_rui.gitee.io/wang_ruis_blog |
李德福 | 完成短信密码找回，完成resutful风格接口 | 基本不会 | https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://ge_shu_ying.gitee.io/hugo_blog |
孙维妙 | 完善客服聊天功能 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/myflask.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |



day07 06-07

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 课程评论功能 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 富文本编辑器+评论情感分析 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 码云，钉钉，微博 第三方登录套娃模式 | 注意回调地址 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 课程评论功能 | 暂时没有 | https://gitee.com/wang_xiao_rui/mydjango_dev.git    
https://gitee.com/wang_xiao_rui/myvue_dev.git
https://wang_xiao_rui.gitee.io/wang_ruis_blog |
李德福 | 完成短信密码找回，完成resutful风格接口 | 基本不会 | https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://ge_shu_ying.gitee.io/hugo_blog |
孙维妙 | 添加课程&课程评论 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |



day07 06-05

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 百度智能云aip | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 提取标签 & MongoDB增查 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 码云，钉钉，微博 第三方登录套娃模式 | 注意回调地址 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 |Rbac权限设置 | 暂时没有 | https://gitee.com/wang_xiao_rui/mydjango_dev.git    
https://gitee.com/wang_xiao_rui/myvue_dev.git
https://wang_xiao_rui.gitee.io/wang_ruis_blog |
李德福 | 完成短信密码找回，完成resutful风格接口 | 基本不会 | https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://ge_shu_ying.gitee.io/hugo_blog |
孙维妙 | 针对课程增删改查(mongo) 对课程标签进行管理(mongo) | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |


day06 06-04

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 提取标签 & MongoDB增查 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 提取标签 & MongoDB增查 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 码云，钉钉，微博 第三方登录套娃模式 | 注意回调地址 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 |课程增删改查(mongo) | 暂时没有 | https://gitee.com/wang_xiao_rui/mydjango_dev.git    
https://gitee.com/wang_xiao_rui/myvue_dev.git
https://wang_xiao_rui.gitee.io/wang_ruis_blog |
李德福 | 完成短信密码找回，完成resutful风格接口 | 基本不会 | https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://ge_shu_ying.gitee.io/hugo_blog |
孙维妙 | 针对课程增删改查(mongo) 对课程标签进行管理(mongo) | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |


day05 06-03

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 无限极分类+递归 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 无极限递归分类 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 码云，钉钉，微博 第三方登录套娃模式 | 注意回调地址 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 |递归思想 无限极分类 | 暂时没有 | https://gitee.com/wang_xiao_rui/mydjango_dev.git    
https://gitee.com/wang_xiao_rui/myvue_dev.git
https://wang_xiao_rui.gitee.io/wang_ruis_blog |
李德福 | 完成短信密码找回，完成resutful风格接口 | 基本不会 | https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://ge_shu_ying.gitee.io/hugo_blog |
孙维妙 | 使用递归实现面试题&无限极分类 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |


day04 06-02

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 集成新浪 钉钉 gitee三个三方登录模块 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 集成新浪 钉钉 gitee三个三方登录模块 | 微博登录报错 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 码云，钉钉，微博 第三方登录套娃模式 | 注意回调地址 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 |集成新浪 钉钉 gitee三个三方登录模块 采用工厂模式对登录类进行复用 | 暂时没有 | https://gitee.com/wang_xiao_rui/mydjango_dev.git    
https://gitee.com/wang_xiao_rui/myvue_dev.git
https://wang_xiao_rui.gitee.io/wang_ruis_blog |
李德福 | 完成短信密码找回，完成resutful风格接口 | 基本不会 | https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://ge_shu_ying.gitee.io/hugo_blog |
孙维妙 | 集成新浪 钉钉 gitee三个三方登录模块
采用工厂模式对登录类进行复用 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |



day03 06-01


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 用celery完成异步发送验证码功能以及定时任务功能 | 当使用post方法时，应注意celery启动命令应由celery worker -A celery_task -l info -P eventlet更改为celery worker -A celery_task -l info -P=solo才可使用post方法 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 利用定时任务写类日志 celery原理流程图 | 上午 异步传参问题 已解决 解决方法 导入app @app.task就可以传参 下午 调用异步任务 因为上午的错导致下午的错 解决方法 所有异步任务都是用@task或@app.task 不能两种混用 | https://gitee.com/wang_xiao_rui/mydjango_dev.git    
https://gitee.com/wang_xiao_rui/myvue_dev.git
https://wang_xiao_rui.gitee.io/wang_ruis_blog |
李德福 | 完成短信密码找回，完成resutful风格接口 | 基本不会 | https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://ge_shu_ying.gitee.io/hugo_blog |
孙惟妙 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |


day02 05-29


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://ge_shu_ying.gitee.io/hugo_blog |
李德福 | 完成短信密码找回，完成resutful风格接口 | 基本不会 | https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://ge_shu_ying.gitee.io/hugo_blog |
孙维妙 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |




day01 05-28


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | django：https://gitee.com/wang_xiao_rui/mydjango_dev.git
vue:https://gitee.com/wang_xiao_rui/myvue_dev.git
博客:https://wang_xiao_rui.gitee.io/wang_ruis_blog |
李德福 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
孙维妙 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |