day08 06-07

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成评论感情分析 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 完成rbac权限系统 添加课程评论 | 暂时没有 | vue: https://gitee.com/l3210440292/my_vue_dev/tree/dev/
django:  https://gitee.com/l3210440292/my_django_dev/tree/dev/
blog： https://www.alfred-alan.online/ |
孙帅 | 添加评论功能 | 暂时没有 | vue:  https://gitee.com/s_s_h/myvue.git
django: https://gitee.com/s_s_h/mydjango.git
blog：http://s_s_h.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 | 完成rbac权限系统 添加课程评论  | 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 使用工厂模式完成三方登陆 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |    https://gitee.com/yy20010514/my-django.git        https://gitee.com/yy20010514/my-vue.git  
高伟鹏 | 制作课程管理页面，针对课程增删改查 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |

day07 06-05

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成Rbac权限设置 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 完成rbac权限系统 添加课程评论 | 暂时没有 | vue: https://gitee.com/l3210440292/my_vue_dev/tree/dev/
django:  https://gitee.com/l3210440292/my_django_dev/tree/dev/
blog： https://www.alfred-alan.online/ |
孙帅 | 完成Rbac权限设置 | 暂时没有 | vue:  https://gitee.com/s_s_h/myvue.git
django: https://gitee.com/s_s_h/mydjango.git
blog：http://s_s_h.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 | 利用mongo实现课程的增删改查 | 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 使用工厂模式完成三方登陆 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |    https://gitee.com/yy20010514/my-django.git        https://gitee.com/yy20010514/my-vue.git  
高伟鹏 | 制作课程管理页面，针对课程增删改查 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |


day06 06-04

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成对课程的增删改查 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 课程的增删改查 | 暂时没有 | vue: https://gitee.com/l3210440292/my_vue_dev/tree/dev/
django:  https://gitee.com/l3210440292/my_django_dev/tree/dev/
blog： https://www.alfred-alan.online/ |
孙帅 | 课程的增删改查 | 暂时没有 | vue:  https://gitee.com/s_s_h/myvue.git
django: https://gitee.com/s_s_h/mydjango.git
blog：http://s_s_h.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 | 利用mongo实现课程的增删改查 | 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 使用工厂模式完成三方登陆 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |    https://gitee.com/yy20010514/my-django.git        https://gitee.com/yy20010514/my-vue.git  
高伟鹏 | 制作课程管理页面，针对课程增删改查 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |

day05 06-03

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 进行无限极数据处理，前端递归展示 | 对于前端递归理解不够透彻 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 展示无限极分类 | 暂时没有 | vue: https://gitee.com/l3210440292/my_vue_dev/tree/dev/
django:  https://gitee.com/l3210440292/my_django_dev/tree/dev/
blog： https://www.alfred-alan.online/ |
孙帅 | 递归无限极分类树 | 暂时没有 | vue:  https://gitee.com/s_s_h/myvue.git
django: https://gitee.com/s_s_h/mydjango.git
blog：http://s_s_h.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 | 进行无限极数据处理，前端递归展示 | 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 使用工厂模式完成三方登陆 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |    https://gitee.com/yy20010514/my-django.git        https://gitee.com/yy20010514/my-vue.git  
高伟鹏 | 利用递归实现高斯求和斐波那契阶乘和课程分类 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |

day04 06-02

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 利用工厂模式完成微博、钉钉、码云的第三放登录 | 对于工厂模式理解不够深入 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 结合工厂模式实现第三方登录 | 暂时没有 | vue: https://gitee.com/l3210440292/my_vue_dev/tree/dev/
思路图:https://gitee.com/l3210440292/my_django_dev/blob/dev/another_login.png
django:  https://gitee.com/l3210440292/my_django_dev/tree/dev/
blog： https://www.alfred-alan.online/ |
孙帅 | 第三方登录+工厂模式 | gitee存储空间发生改变，重新建立连接后新写的代码丢失 | vue:  https://gitee.com/sun_shuai_gitee/myvue.git
django: https://gitee.com/sun_shuai_gitee/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 | 集成新浪 钉钉 gitee三个三方登录模块 采用工厂模式对登录类进行复用 完成oauth2的登录流程图绘制 | 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 使用工厂模式完成三方登陆 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |    https://gitee.com/yy20010514/my-django.git        https://gitee.com/yy20010514/my-vue.git  
高伟鹏 | 集成新浪 钉钉 gitee三个三方登录模块 采用工厂模式对登录类进行复用 完成oauth2的登录流程图绘制 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |

day03 06-01


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | celery异步发送邮件，异步发送短信，异步定时任务 | 1.异步处理post请求报错（已处理）2.异步请求KeyError（已处理） | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 完成异步邮件发送 定时写log日志 | 暂时没有 | vue: https://gitee.com/l3210440292/my_vue_dev/tree/dev/
django:  https://gitee.com/l3210440292/my_django_dev/tree/dev/
blog： https://www.alfred-alan.online/ |
孙帅 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue:  https://gitee.com/sun_shuai_gitee/myvue.git
django: https://gitee.com/sun_shuai_gitee/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |
高伟鹏 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |


day02 05-29


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
孙帅 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue:  https://gitee.com/sun_shuai_gitee/myvue.git
django: https://gitee.com/sun_shuai_gitee/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |
高伟鹏 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |



day01 05-28


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
孙帅 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue:  https://gitee.com/sun_shuai_gitee/myvue.git
django: https://gitee.com/sun_shuai_gitee/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |
高伟鹏 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |


