day06 06-04

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成后端异步上传视频文件 | 暂时没有 | https://gitee.com/i_xiao_luo/my_django django<br>https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成后端异步上传视频文件 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev<br>https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
王增辉 | 完成后端异步上传视频文件 | 无 | django地址：https://gitee.com/wang_zeng_hui/mydjango_dev<br>vue地址：https://gitee.com/wang_zeng_hui/myvue_dev
https://wang_zeng_hui.gitee.io/ |
王鑫 | 完成后端异步上传视频文件| 没有 | https://gitee.com/wxb_857/big_mydjango<br>https://gitee.com/wxb_857/big_myvue
https://wxb_857.gitee.io/blog/ |
王涵 | 完成后端异步上传视频文件 | 暂时没有 | https://gitee.com/wanghan123456/mydjango_dev<br>https://gitee.com/wanghan123456/myvue_dev
https://wanghan123-github.github.io/ |
聂明华 | 递归无限极分类 | 暂无 | https://gitee.com/nieminghua/mydjango<br>https://gitee.com/nieminghua/myvue<br>https://nieminghua.gitee.io/blog/ |** 


day05 06-03

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 实现了无限极分类，并根据文档做出了前端展示 | vue展示不太明白 | https://gitee.com/i_xiao_luo/my_django django
https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 使用工厂模式完成三方登录 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
王增辉 | 无限极分类和基础递归算法 | 无限极分类vue展示不会 | django地址：
https://gitee.com/wang_zeng_hui/mydjango_dev
vue地址：
https://gitee.com/wang_zeng_hui/myvue_dev
博客地址：
https://wang_zeng_hui.gitee.io/ |
王鑫 | 了解递归算法的思想| 没有 | https://gitee.com/wxb_857/big_mydjango
https://gitee.com/wxb_857/big_myvue
https://wxb_857.gitee.io/blog/ |
王涵 | 基础算法和递归无限极分类 | 消化的还不够好 | https://gitee.com/wanghan123456/mydjango_dev
https://gitee.com/wanghan123456/myvue_dev
https://wanghan123-github.github.io/ |
聂明华 | 递归无限极分类 | 暂无 | https://gitee.com/nieminghua/mydjango<br>https://gitee.com/nieminghua/myvue<br>https://nieminghua.gitee.io/blog/ |** 


day04 06-02

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 使用工厂模式完成三方登录 | 没有 | https://gitee.com/i_xiao_luo/my_django django
https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 使用工厂模式完成三方登录 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
王增辉 | 使用工厂模式完成第三方登录 | 对工厂模式不怎么熟练 | django地址：
https://gitee.com/wang_zeng_hui/mydjango_dev
vue地址：
https://gitee.com/wang_zeng_hui/myvue_dev
博客地址：
https://wang_zeng_hui.gitee.io/ |
王鑫 | 使用工厂模式完成三方登录| 对工厂模式的执行流程的认识有点模糊 | https://gitee.com/wxb_857/big_mydjango
https://gitee.com/wxb_857/big_myvue
https://wxb_857.gitee.io/blog/ |
王涵 | 根据文档完成码云第三方登录 | 对工厂模式的应用不是很熟练 | https://gitee.com/wanghan123456/mydjango_dev
https://gitee.com/wanghan123456/myvue_dev
http://kc0v34.coding-pages.com/ |
聂明华 | 第三方登陆工程模式 | 无 | https://gitee.com/nieminghua/mydjango<br>https://gitee.com/nieminghua/myvue<br>https://nieminghua.gitee.io/blog/ |** 


day03 06-01

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成短信密码找回，完成resutful风格接口 | 没有 | https://gitee.com/i_xiao_luo/my_django django
https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
王增辉 | 了解celery原理，使用异步发邮件及短信和定时功能 | 写在博客里| django地址：
https://gitee.com/wang_zeng_hui/mydjango_dev
vue地址：
https://gitee.com/wang_zeng_hui/myvue_dev
博客地址：
https://wang_zeng_hui.gitee.io/ |
王鑫 | 使用celery异步使用邮箱验证和短信发送功能 | 问题存在于启动程序时要注意你的请求方式是否为post，如果是你需要修改启动命令 | https://gitee.com/wxb_857/big_mydjango
https://gitee.com/wxb_857/big_myvue
https://wxb_857.gitee.io/blog/ |
王涵 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wanghan123456/mydjango_dev
https://gitee.com/wanghan123456/myvue_dev
http://kc0v34.coding-pages.com/ |
聂明华 | 完成邮件短信异步任务改造<br>定时任务写入文件 | 启动服务器错误<br>requests包的requests.post发送后，传不回数据<br>命令需更换为 celery worker -A mydjango -l info  --pool=solo | https://gitee.com/nieminghua/mydjango<br>https://gitee.com/nieminghua/myvue<br>https://nieminghua.gitee.io/blog/ |** 

day02 05-29

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/i_xiao_luo/my_django django
https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 5 |
王增辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | django地址：
https://gitee.com/wang_zeng_hui/mydjango_dev
vue地址：
https://gitee.com/wang_zeng_hui/myvue_dev
博客地址：
https://wang_zeng_hui.gitee.io/ |
王鑫 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 5 |
王涵 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wanghan123456/mydjango_dev
https://gitee.com/wanghan123456/myvue_dev
http://kc0v34.coding-pages.com/ |
暂时没有 | https://gitee.com/nieminghua/mydjango
https://gitee.com/nieminghua/myvue
https://nieminghua.gitee.io/blog/ |



day01 05-28


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成短信密码找回，完成resutful风格接口 | 没有 | https://gitee.com/i_xiao_luo/my_django django
https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
王增辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | django地址：
https://gitee.com/wang_zeng_hui/mydjango_dev
vue地址：
https://gitee.com/wang_zeng_hui/myvue_dev
博客地址：
https://wang_zeng_hui.gitee.io/ |
王鑫 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
王涵 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wanghan123456/mydjango123
https://gitee.com/wanghan123456/myvue123
http://wanghan123456.gitee.io/my_technology_blog |
聂明华 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/nieminghua/mydjango
https://gitee.com/nieminghua/myvue
https://nieminghua.gitee.io/blog/ |