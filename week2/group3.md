day06 06-07


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
姜枨文 | 实现gitee，sina三方登录工厂模式| 静态方法调用一开始没理解 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 课程评论以及其他优化 | 暂时没有 | vue:https://gitee.com/wangxs020202/edu_vue/branches                                                     django:https://gitee.com/wangxs020202/edu_dj/branches                                                                  博客：https://www.sirxs.cn/ |
田文艳 |基于MongoDB实现课程与标签的增删改查 | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/tian_wen_yan/mydjango1.git |
郭其鑫 | 课程评论以及其他功能 | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 课程评论以及其他优化 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 设置权限功能 | 暂时没有 | http://wang_chaoo.gitee.io/my_technology_blog/
https://gitee.com/wang_chaoo/mydjango_dev
https://gitee.com/wang_chaoo/myvue_dev |


day06 06-04


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
姜枨文 | 实现gitee，sina三方登录工厂模式| 静态方法调用一开始没理解 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 基于MongoDB实现课程与标签的增删改查 | 暂时没有 | vue:https://gitee.com/wangxs020202/edu_vue/branches                                                     django:https://gitee.com/wangxs020202/edu_dj/branches                                                                  博客：https://www.sirxs.cn/ |
田文艳 |基于MongoDB实现课程与标签的增删改查 | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/tian_wen_yan/mydjango1.git |
郭其鑫 | 无限极分类和递归 | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 基于MongoDB实现课程与标签的增删改查 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 基于MongoDB实现课程与标签的增删改查 | 暂时没有 | http://wang_chaoo.gitee.io/my_technology_blog/
https://gitee.com/wang_chaoo/mydjango_dev
https://gitee.com/wang_chaoo/myvue_dev |

day05 06-03


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
姜枨文 | 实现gitee，sina三方登录工厂模式| 静态方法调用一开始没理解 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 实现无限极递归分类 | 暂时没有 | vue:https://gitee.com/wangxs020202/edu_vue/branches                                                     django:https://gitee.com/wangxs020202/edu_dj/branches                                                                  博客：https://www.sirxs.cn/ |
田文艳 |无限极分类和递归 | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/tian_wen_yan/mydjango1.git |
郭其鑫 | 无限极分类和递归 | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 无限极分类和递归 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 无限极分类和递归 | 暂时没有 | http://wang_chaoo.gitee.io/my_technology_blog/
https://gitee.com/wang_chaoo/mydjango_dev
https://gitee.com/wang_chaoo/myvue_dev |

day04 06-02


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
姜枨文 | 实现gitee，sina三方登录工厂模式| 静态方法调用一开始没理解 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 实现钉钉,微博,码云三方登录 以及工厂模式复用 oauth2登陆流程图 | 暂时没有 | vue:https://gitee.com/wangxs020202/edu_vue/branches                                                     django:https://gitee.com/wangxs020202/edu_dj/branches                                                                  博客：https://www.sirxs.cn/ |
田文艳 | 第三方登陆 | 钉钉登录时url不合法 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/tian_wen_yan/mydjango1.git |
郭其鑫 | 完成git 钉钉 微博三方登陆 | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 实现钉钉,微博,码云三方登录 以及工厂模式复用 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 实现钉钉,微博,码云三方登录 | 暂时没有 | http://wang_chaoo.gitee.io/my_technology_blog/
https://gitee.com/wang_chaoo/mydjango_dev
https://gitee.com/wang_chaoo/myvue_dev |

day02 05-29


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
蒋枨文 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue:https://gitee.com/wangxs020202/edu_vue/commits/develop                                                      django:https://gitee.com/wangxs020202/edu_dj/tree/develop/                                                                  博客：https://www.sirxs.cn/ |
田文艳 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/tian_wen_yan/mydjango1.git |
郭其鑫 | 完成git 钉钉 微博三方登陆 | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |



day01 05-28

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
蒋枨文 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue:https://gitee.com/wangxs020202/edu_vue/commits/develop                                                      django:https://gitee.com/wangxs020202/edu_dj/tree/develop/                                                                  博客：https://www.sirxs.cn/ |
田文艳 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/tian_wen_yan/mydjango1.git |
郭其鑫 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |