day10 07-12

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 消息盒子 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/myflask
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 购物车 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 秒杀杀杀杀杀杀杀杀 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 数据分析| 暂时没有 | https://gitee.com/wang_xiao_rui/myflask_dev.git
博客:https://wang_xiao_rui.gitee.io/wang_ruis_blog|
李德福 | 定时推送消息 | 暂时没有 | https://gitee.com/justblue/flask1education https://www.hear2ken.cn |
孙维妙 | 设计分布式订单系统 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |

day10 07-11


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 消息盒子 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/myflask
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 购物车 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 秒杀杀杀杀杀杀杀杀 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 数据分析| 暂时没有 | https://gitee.com/wang_xiao_rui/myflask_dev.git
博客:https://wang_xiao_rui.gitee.io/wang_ruis_blog|
李德福 | 消息盒子 | 暂时没有 |  https://gitee.com/justblue/myvue_dev/tree/dev/ https://www.hear2ken.cn |
孙维妙 | 设计分布式订单系统 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |


day10 06-14


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 分布式订单页 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 购物车 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 秒杀杀杀杀杀杀杀杀 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 分布式订单| 暂时没有 | django：https://gitee.com/wang_xiao_rui/mydjango_dev.git
vue:https://gitee.com/wang_xiao_rui/myvue_dev.git
博客:https://wang_xiao_rui.gitee.io/wang_ruis_blog|
李德福 | sowflake发号器 | 暂时没有 | https://gitee.com/justblue/mydjango_master/tree/dev/ https://gitee.com/justblue/myvue_dev/tree/dev/ https://www.hear2ken.cn |
孙维妙 | 设计分布式订单系统 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |


day10 06-11


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 商品秒杀 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 购物车 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 秒杀杀杀杀杀杀杀杀 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 秒杀商品 | 暂时没有 | django：https://gitee.com/wang_xiao_rui/mydjango_dev.git
vue:https://gitee.com/wang_xiao_rui/myvue_dev.git
博客:https://wang_xiao_rui.gitee.io/wang_ruis_blog|
李德福 | 促销系统 | 暂时没有 | https://gitee.com/justblue/mydjango_master/tree/dev/ https://gitee.com/justblue/myvue_dev/tree/dev/ https://www.hear2ken.cn |
孙维妙 | 秒杀系统 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |



day10 06-10


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 单点登录 & 上下文管理封装 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 购物车 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | redis储存购物车信息 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 单点登录 & 上下文管理封装 | 暂时没有 | django：https://gitee.com/wang_xiao_rui/mydjango_dev.git
vue:https://gitee.com/wang_xiao_rui/myvue_dev.git
博客:https://wang_xiao_rui.gitee.io/wang_ruis_blog|
李德福 | SSO+Oauth2+JWT单点登录 | 暂时没有 | https://gitee.com/justblue/mydjango_master/tree/dev/ https://gitee.com/justblue/myvue_dev/tree/dev/ https://www.hear2ken.cn |
孙维妙 | 单点登录 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |


day09 06-9


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 又拍云的多线程分块文件上传 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 购物车 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | redis储存购物车信息 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 又拍云文件上传 | 暂时没有 | django：https://gitee.com/wang_xiao_rui/mydjango_dev.git
vue:https://gitee.com/wang_xiao_rui/myvue_dev.git
博客:https://wang_xiao_rui.gitee.io/wang_ruis_blog|
李德福 | redis+后端购物车系统 | 暂时没有 | https://gitee.com/justblue/mydjango_master/tree/dev/ https://gitee.com/justblue/myvue_dev/tree/dev/ https://www.hear2ken.cn |
孙维妙 | 又拍云多线程分块上传 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |



day09 06-8


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | redis购物车系统 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 购物车 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | redis储存购物车信息 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 购物车系统 | 暂时没有 | django：https://gitee.com/wang_xiao_rui/mydjango_dev.git
vue:https://gitee.com/wang_xiao_rui/myvue_dev.git
博客:https://wang_xiao_rui.gitee.io/wang_ruis_blog|
李德福 | redis+后端购物车系统 | 暂时没有 | https://gitee.com/justblue/mydjango_master/tree/dev/ https://gitee.com/justblue/myvue_dev/tree/dev/ https://www.hear2ken.cn |
孙惟妙 | 使用redis后端重构购物车系统 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |


day09 06-7


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 完成情感分析结合评论 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 完成情感分析结合评论 | 暂时没有 | django：https://gitee.com/wang_xiao_rui/mydjango_dev.git
vue:https://gitee.com/wang_xiao_rui/myvue_dev.git
博客:https://wang_xiao_rui.gitee.io/wang_ruis_blog|
李德福 | 完成baidu-NLP的智能分析评论、基于mongo对课程的restful接口、| my vue is so poor | 
https://gitee.com/justblue/mydjango_master/tree/dev/ https://gitee.com/justblue/myvue_dev/tree/dev/ https://www.hear2ken.cn |
孙惟妙 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |



day02 05-29


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://ge_shu_ying.gitee.io/hugo_blog |
李德福 | 完成短信密码找回，完成resutful风格接口 | 基本不会 | https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://ge_shu_ying.gitee.io/hugo_blog |
孙惟妙 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |




day01 05-28


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | django：https://gitee.com/wang_xiao_rui/mydjango_dev.git
vue:https://gitee.com/wang_xiao_rui/myvue_dev.git
博客:https://wang_xiao_rui.gitee.io/wang_ruis_blog |
李德福 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
孙惟妙 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |