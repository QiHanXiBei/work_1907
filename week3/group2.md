day014 06-15

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成分布式订单存储 | 暂时没有 | https://gitee.com/i_xiao_luo/my_django django<br>https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成分布式订单存储  | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev<br>https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
王增辉 | 完成签名机制 | 没有 | django地址：https://gitee.com/wang_zeng_hui/mydjango_dev<br>vue地址：https://gitee.com/wang_zeng_hui/myvue_dev
https://wang_zeng_hui.gitee.io/ |
王鑫 | 完成分布式订单存储| 没有 | https://gitee.com/wxb_857/big_mydjango<br>https://gitee.com/wxb_857/big_myvue
https://wxb_857.gitee.io/blog/ |
王涵 | 完成分布式订单存储 | 暂时没有 | https://gitee.com/wanghan123456/mydjango_dev<br>https://gitee.com/wanghan123456/myvue_dev
https://wanghan123-github.github.io/ |
聂明华 | 完成分布式订单存储 | 暂无 | https://gitee.com/nieminghua/mydjango<br>https://gitee.com/nieminghua/myvue<br>https://nieminghua.gitee.io/blog/ |** 


day013 06-14

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成分布式订单存储 | 暂时没有 | https://gitee.com/i_xiao_luo/my_django django<br>https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成分布式订单存储  | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev<br>https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
王增辉 | 完成分布式订单 | 没有 | django地址：https://gitee.com/wang_zeng_hui/mydjango_dev<br>vue地址：https://gitee.com/wang_zeng_hui/myvue_dev
https://wang_zeng_hui.gitee.io/ |
王鑫 | 完成分布式订单存储| 没有 | https://gitee.com/wxb_857/big_mydjango<br>https://gitee.com/wxb_857/big_myvue
https://wxb_857.gitee.io/blog/ |
王涵 | 完成分布式订单存储 | 暂时没有 | https://gitee.com/wanghan123456/mydjango_dev<br>https://gitee.com/wanghan123456/myvue_dev
https://wanghan123-github.github.io/ |
聂明华 | 完成分布式订单存储 | 暂无 | https://gitee.com/nieminghua/mydjango<br>https://gitee.com/nieminghua/myvue<br>https://nieminghua.gitee.io/blog/ |** 



day012 06-11

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成秒杀 | 暂时没有 | https://gitee.com/i_xiao_luo/my_django django<br>https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成秒杀商品的多并发操作  | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev<br>https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
王增辉 | 完成秒杀 | 没有 | django地址：https://gitee.com/wang_zeng_hui/mydjango_dev<br>vue地址：https://gitee.com/wang_zeng_hui/myvue_dev
https://wang_zeng_hui.gitee.io/ |
王鑫 | 完成秒杀解压操作| 没有 | https://gitee.com/wxb_857/big_mydjango<br>https://gitee.com/wxb_857/big_myvue
https://wxb_857.gitee.io/blog/ |
王涵 | 完成秒杀商品 | 暂时没有 | https://gitee.com/wanghan123456/mydjango_dev<br>https://gitee.com/wanghan123456/myvue_dev
https://wanghan123-github.github.io/ |
聂明华 | 高并发防止超卖 | 暂无 | https://gitee.com/nieminghua/mydjango<br>https://gitee.com/nieminghua/myvue<br>https://nieminghua.gitee.io/blog/ |** 



day011 06-10

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成单点登录 | 暂时没有 | https://gitee.com/i_xiao_luo/my_django django<br>https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成单点登录  | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev<br>https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
王增辉 | 微服务  | 不太会 | django地址：https://gitee.com/wang_zeng_hui/mydjango_dev<br>vue地址：https://gitee.com/wang_zeng_hui/myvue_dev
https://wang_zeng_hui.gitee.io/ |
王鑫 | 完成单点登录| 没有 | https://gitee.com/wxb_857/big_mydjango<br>https://gitee.com/wxb_857/big_myvue
https://wxb_857.gitee.io/blog/ |
王涵 | 完成微服务 | 暂时没有 | https://gitee.com/wanghan123456/mydjango_dev<br>https://gitee.com/wanghan123456/myvue_dev
https://wanghan123-github.github.io/ |
聂明华 | 单点登陆 | 暂无 | https://gitee.com/nieminghua/mydjango<br>https://gitee.com/nieminghua/myvue<br>https://nieminghua.gitee.io/blog/ |** 


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/i_xiao_luo/my_django django
https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 5 |
王增辉 | 微服务 | 不太会 | django地址：
https://gitee.com/wang_zeng_hui/mydjango_dev
vue地址：
https://gitee.com/wang_zeng_hui/myvue_dev
博客地址：
http://www.okwzh.cn|
王鑫 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 5 |
王涵 | 完成又拍云分块上传 | 暂时没有 | https://gitee.com/wanghan123456/mydjango_dev
https://gitee.com/wanghan123456/myvue_dev
https://wanghan123-github.github.io/ |
暂时没有 | https://gitee.com/nieminghua/mydjango
https://gitee.com/nieminghua/myvue
https://nieminghua.gitee.io/blog/ |



day010 06-9

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/i_xiao_luo/my_django django
https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 5 |
王增辉 | 又拍云分块上传文件 | 暂时没有 | django地址：
https://gitee.com/wang_zeng_hui/mydjango_dev
vue地址：
https://gitee.com/wang_zeng_hui/myvue_dev
博客地址：
http://www.okwzh.cn|
王鑫 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 5 |
王涵 | 完成又拍云分块上传 | 暂时没有 | https://gitee.com/wanghan123456/mydjango_dev
https://gitee.com/wanghan123456/myvue_dev
https://wanghan123-github.github.io/ |
暂时没有 | https://gitee.com/nieminghua/mydjango
https://gitee.com/nieminghua/myvue
https://nieminghua.gitee.io/blog/ |


day09 06-8

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/i_xiao_luo/my_django django
https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 5 |
王增辉 | 完成购物车功能和存储redis | 暂时没有 | django地址：
https://gitee.com/wang_zeng_hui/mydjango_dev
vue地址：
https://gitee.com/wang_zeng_hui/myvue_dev
博客地址：
http://www.okwzh.cn|
王鑫 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 5 |
王涵 | 完成redis购物车 | 暂时没有 | https://gitee.com/wanghan123456/mydjango_dev
https://gitee.com/wanghan123456/myvue_dev
https://wanghan123-github.github.io/ |
暂时没有 | https://gitee.com/nieminghua/mydjango
https://gitee.com/nieminghua/myvue
https://nieminghua.gitee.io/blog/ |

day08 06-7

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/i_xiao_luo/my_django django
https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 5 |
王增辉 | 添加评论功能，富文本，和ai情感处理 | 暂时没有 | django地址：
https://gitee.com/wang_zeng_hui/mydjango_dev
vue地址：
https://gitee.com/wang_zeng_hui/myvue_dev
博客地址：
http://www.okwzh.cn|
王鑫 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 5 |
王涵 | 课程评论，富文本编辑器，ai情感处理， | 权限管理这块思路不顺 | https://gitee.com/wanghan123456/mydjango_dev
https://gitee.com/wanghan123456/myvue_dev
https://wanghan123-github.github.io/ |
暂时没有 | https://gitee.com/nieminghua/mydjango
https://gitee.com/nieminghua/myvue
https://nieminghua.gitee.io/blog/ |


day02 05-29

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/i_xiao_luo/my_django django
https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 5 |
王增辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | django地址：
https://gitee.com/wang_zeng_hui/mydjango_dev
vue地址：
https://gitee.com/wang_zeng_hui/myvue_dev
博客地址：
https://wang_zeng_hui.gitee.io/ |
王鑫 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 5 |
王涵 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wanghan123456/mydjango123
https://gitee.com/wanghan123456/myvue123
http://wanghan123456.gitee.io/my_technology_blog |
暂时没有 | https://gitee.com/nieminghua/mydjango
https://gitee.com/nieminghua/myvue
https://nieminghua.gitee.io/blog/ |



day01 05-28


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成短信密码找回，完成resutful风格接口 | 没有 | https://gitee.com/i_xiao_luo/my_django django
https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
王增辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | django地址：
https://gitee.com/wang_zeng_hui/mydjango_dev
vue地址：
https://gitee.com/wang_zeng_hui/myvue_dev
博客地址：
https://wang_zeng_hui.gitee.io/ |
王鑫 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
王涵 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wanghan123456/mydjango123
https://gitee.com/wanghan123456/myvue123
http://wanghan123456.gitee.io/my_technology_blog |
聂明华 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/nieminghua/mydjango
https://gitee.com/nieminghua/myvue
https://nieminghua.gitee.io/blog/ |