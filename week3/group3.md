day13 06-12


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
蒋枨文 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 完成订单功能 | 暂时没有 | vue:https://gitee.com/wangxs020202/edu_vue/branches django:https://gitee.com/wangxs020202/edu_dj/branches 博客：https://www.sirxs.cn/ |
田文艳 | 订单 | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/tian_wen_yan/mydjango1.git |
郭其鑫 | 完成秒杀 | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 完成秒杀 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 完成秒杀 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |


day12 06-11


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
蒋枨文 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 完成秒杀 | 暂时没有 | vue:https://gitee.com/wangxs020202/edu_vue/branches django:https://gitee.com/wangxs020202/edu_dj/branches 博客：https://www.sirxs.cn/ |
田文艳 | 完成秒杀功能 | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/tian_wen_yan/mydjango1.git |
郭其鑫 | 完成秒杀 | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 完成秒杀 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 完成秒杀 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |

day11 06-10


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
蒋枨文 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 完成单点登录 | 暂时没有 | vue:https://gitee.com/wangxs020202/edu_vue/branches django:https://gitee.com/wangxs020202/edu_dj/branches 博客：https://www.sirxs.cn/ |
田文艳 | 完成单点登录 | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/tian_wen_yan/mydjango1.git |
郭其鑫 | 完成单点登录 | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 实现单点登录 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 又拍云异步多线程上传文件 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |

day10 06-09


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
蒋枨文 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 完成又拍云异步多线程上传视频文件 | 暂时没有 | vue:https://gitee.com/wangxs020202/edu_vue/branches django:https://gitee.com/wangxs020202/edu_dj/branches 博客：https://www.sirxs.cn/ |
田文艳 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/tian_wen_yan/mydjango1.git |
郭其鑫 | 完成又拍云异步多线程上传视频文件 | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 又拍云、七牛云分块上传 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 又拍云异步多线程上传文件 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |


day09 06-08


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
蒋枨文 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 完成购物车功能 | 暂时没有 | vue:https://gitee.com/wangxs020202/edu_vue/branches django:https://gitee.com/wangxs020202/edu_dj/branches 博客：https://www.sirxs.cn/ |
田文艳 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/tian_wen_yan/mydjango1.git |
郭其鑫 | 完成短信 邮箱异步请求，每隔两小时定时任务创建一个文件 时间点吸入日志 | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 添加购物车功能 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 添加购物车功能 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |


day02 05-29


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
蒋枨文 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue:https://gitee.com/wangxs020202/edu_vue/commits/develop                                                      django:https://gitee.com/wangxs020202/edu_dj/tree/develop/                                                                  博客：https://www.sirxs.cn/ |
田文艳 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/tian_wen_yan/mydjango1.git |
郭其鑫 | 完成短信 邮箱异步请求，每隔两小时定时任务创建一个文件 时间点吸入日志 | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 构建rbac权限认证体系 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |



day01 05-28

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
蒋枨文 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue:https://gitee.com/wangxs020202/edu_vue/commits/develop                                                      django:https://gitee.com/wangxs020202/edu_dj/tree/develop/                                                                  博客：https://www.sirxs.cn/ |
田文艳 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/tian_wen_yan/mydjango1.git |
郭其鑫 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 课程管理 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |