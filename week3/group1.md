
day07 06-14

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成分布式存储订单 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 完成分布式存储订单 | 暂时没有 | vue: https://gitee.com/l3210440292/my_vue_dev/tree/dev/
django: https://gitee.com/l3210440292/my_django_dev/tree/dev/
blog： https://www.alfred-alan.online/|
孙帅 | 分布式存储订单 | 暂时没有 | vue:  https://gitee.com/sun_shuai_gitee/myvue.git
django: https://gitee.com/sun_shuai_gitee/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | snowflake服务无法启动 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 | 完成分布式存储订单 | 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 限时秒杀,分布式存储订单 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |  https://gitee.com/yy20010514/my-vue.git   |https://gitee.com/yy20010514/my-django.git
高伟鹏 | 完成分布式存储订单 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |

day04 06-11

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 实现限时秒杀 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 解决高并发读写问题 | 暂时没有 | vue: https://gitee.com/l3210440292/my_vue_dev/tree/dev/
django: https://gitee.com/l3210440292/my_django_dev/tree/dev/
blog： https://www.alfred-alan.online/ |
孙帅 | 产品秒杀，高并发读写 | 暂时没有 | vue:  https://gitee.com/sun_shuai_gitee/myvue.git
django: https://gitee.com/sun_shuai_gitee/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 | 实现限时秒杀| 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 单点登陆,限时秒杀 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |  https://gitee.com/yy20010514/my-vue.git   |https://gitee.com/yy20010514/my-django.git
高伟鹏 | 秒杀商品 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |

day03 06-10

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 实现单点登录 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 实现单点登录 | 暂时没有 | vue: https://gitee.com/l3210440292/my_vue_dev/tree/dev/
django: https://gitee.com/l3210440292/my_django_dev/tree/dev/
blog： https://www.alfred-alan.online/ |
孙帅 | 单点登录 | 暂时没有 | vue:  https://gitee.com/sun_shuai_gitee/myvue.git
django: https://gitee.com/sun_shuai_gitee/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 | 实现单点登录| 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 购物车,文件上传 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |  https://gitee.com/yy20010514/my-vue.git   |https://gitee.com/yy20010514/my-django.git
高伟鹏 | 单点登陆&上下文管理封装 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |

day02 06-10


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成异步分块上传 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 实现单点登录 | 暂时没有 | vue: https://gitee.com/l3210440292/my_vue_dev/tree/dev/
django: https://gitee.com/l3210440292/my_django_dev/tree/dev/
blog： https://www.alfred-alan.online/ |
孙帅 | 多线程异步分块上传 | 暂时没有 | vue:  https://gitee.com/sun_shuai_gitee/myvue.git
django: https://gitee.com/sun_shuai_gitee/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 |完成异步分块上传| 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 购物车,文件上传 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |  https://gitee.com/yy20010514/my-vue.git   |https://gitee.com/yy20010514/my-django.git
高伟鹏 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |

day02 06-09


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成异步分块上传 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 完成视频分块上传 | 暂时没有 | vue: https://gitee.com/l3210440292/my_vue_dev/tree/dev/
django: https://gitee.com/l3210440292/my_django_dev/tree/dev/
blog： https://www.alfred-alan.online/ |
孙帅 | 多线程异步分块上传 | 暂时没有 | vue:  https://gitee.com/sun_shuai_gitee/myvue.git
django: https://gitee.com/sun_shuai_gitee/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 |完成异步分块上传| 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 购物车,文件上传 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |  https://gitee.com/yy20010514/my-vue.git   |https://gitee.com/yy20010514/my-django.git
高伟鹏 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |

day02 06-08


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成reids购物车实现 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 完成后端存储购物车功能 | 暂时没有 | vue: https://gitee.com/l3210440292/my_vue_dev/tree/dev/
django: https://gitee.com/l3210440292/my_django_dev/tree/dev/
blog： https://www.alfred-alan.online/ |
孙帅 | redis购物车存储 | 暂时没有 | vue:  https://gitee.com/sun_shuai_gitee/myvue.git
django: https://gitee.com/sun_shuai_gitee/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 |完成后端存储购物车功能| 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 购物车,文件上传 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |  https://gitee.com/yy20010514/my-vue.git   |https://gitee.com/yy20010514/my-django.git
高伟鹏 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |



day01 05-28


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
孙帅 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue:  https://gitee.com/sun_shuai_gitee/myvue.git
django: https://gitee.com/sun_shuai_gitee/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |
高伟鹏 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |


