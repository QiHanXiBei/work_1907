day2 06-28

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
蒋枨文 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 使用websocket实现客服系统 | 暂时没有 | vue:https://gitee.com/wangxs020202/flask_vue/branches flask:https://gitee.com/wangxs020202/flask/branches 博客：https://www.sirxs.cn/ |
田文艳 | 钉钉机器人推送天气 | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/xiaotian18/my_flask.git |
郭其鑫 |使用原生sql查询全部标签， | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 钉钉机器人推送天气 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 使用websocket实现客服系统 | 暂时没有 | https://gitee.com/wang_chaoo/flask_vue
https://gitee.com/wang_chaoo/my_flask
http://wang_chaoo.gitee.io/my_technology_blog/ |


day1 06-27

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
蒋枨文 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 用户认证微服务系统重构 | 暂时没有 | vue:https://gitee.com/wangxs020202/flask_vue/branches flask:https://gitee.com/wangxs020202/flask/branches 博客：https://www.sirxs.cn/ |
田文艳 | websocket实现在线询问' | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/xiaotian18/myvue1.git
https://gitee.com/xiaotian18/my_flask.git |
郭其鑫 |使用原生sql查询全部标签， | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 钉钉机器人推送天气 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 使用celery钉钉机器人定时推送天气 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |