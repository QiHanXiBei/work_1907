day03 07-06

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成socket多房间会话 | 暂时没有 | vue: https://gitee.com/asyx/zhouce
flask: https://gitee.com/asyx/zhouce
blog： https://asyx.gitee.io/bike |
刘吉庆 | 使用socket.io 搭建客服系统 | 暂时没有 | https://gitee.com/l3210440292/flask_vue
flask: https://gitee.com/l3210440292/my_flask_dev
blog： https://www.alfred-alan.online/ |
孙帅 | 使用socket.io 推送消息，聊天室项目移植 | 暂时没有 | vue:  https://gitee.com/s_s_h/myflaskVue.git
flask: https://gitee.com/s_s_h/myflask.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 |完成socket多房间会话| 暂时没有 | https://gitee.com/qishuduo/qiflask 
https://gitee.com/qishuduo/flask_vue
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 |  https://gitee.com/yy20010514/my_flask.git
高伟鹏 | 使用socket.io 搭建客服系统 | 暂时没有 | https://gitee.com/gao_wei_peng/myflask_dev flask https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |


day02 06-30

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成对socket握手过程的理解 | 暂时没有 | vue: https://gitee.com/asyx/zhouce
flask: https://gitee.com/asyx/zhouce
blog： https://asyx.gitee.io/bike |
刘吉庆 | 使用socket.io 搭建客服系统 | 暂时没有 | https://gitee.com/l3210440292/flask_vue
flask: https://gitee.com/l3210440292/my_flask_dev
blog： https://www.alfred-alan.online/ |
孙帅 | 使用socket.io 实现在线咨询系统 | 暂时没有 | vue:  https://gitee.com/s_s_h/myvue.git
flask: https://gitee.com/s_s_h/myflask.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 | 使用socket.io 实现在线咨询系统 | 暂时没有 | https://gitee.com/qishuduo/qiflask 
https://gitee.com/qishuduo/flask_vue
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 |  https://gitee.com/yy20010514/my_flask.git
高伟鹏 | 使用socket.io 搭建客服系统 | 暂时没有 | https://gitee.com/gao_wei_peng/myflask_dev flask https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |


day01 06-28

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成socket.io 搭建客服系统 | 暂时没有 | vue: https://gitee.com/asyx/zhouce
flask: https://gitee.com/asyx/zhouce
blog： https://asyx.gitee.io/bike |
刘吉庆 | 使用socket.io 搭建客服系统 | 暂时没有 | https://gitee.com/l3210440292/flask_vue
flask: https://gitee.com/l3210440292/my_flask_dev
blog： https://www.alfred-alan.online/ |
孙帅 | 使用socket.io 实现在线咨询系统 | 暂时没有 | vue:  https://gitee.com/s_s_h/myvue.git
flask: https://gitee.com/s_s_h/myflask.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 | 使用socket.io 实现在线咨询系统 | 暂时没有 | https://gitee.com/qishuduo/qiflask 
https://gitee.com/qishuduo/flask_vue
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 |  https://gitee.com/yy20010514/my_flask.git
高伟鹏 | 使用socket.io 搭建客服系统 | 暂时没有 | https://gitee.com/gao_wei_peng/myflask_dev flask https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |

day01 06-27

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成flask微服务 | 暂时没有 | vue: https://gitee.com/asyx/zhouce
flask: https://gitee.com/asyx/zhouce
blog： https://asyx.gitee.io/bike |
刘吉庆 | 重构用户微服务认证系统 | 暂时没有 | https://gitee.com/l3210440292/flask_vue
flask: https://gitee.com/l3210440292/my_flask_dev
blog： https://www.alfred-alan.online/ |
孙帅 | 重构用户认证微服务系统 | 暂时没有 | vue:  https://gitee.com/s_s_h/myvue.git
flask: https://gitee.com/s_s_h/myflask.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 | 完成flask微服务 | 暂时没有 | https://gitee.com/qishuduo/qiflask 
https://gitee.com/qishuduo/flask_vue
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 |  https://gitee.com/yy20010514/my_flask.git
高伟鹏 | 重构用户认证微服务系统 | 暂时没有 | https://gitee.com/gao_wei_peng/myflask_dev flask https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |

day02 06-24

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 重构用户微服务认证系统 | 暂时没有 | https://gitee.com/l3210440292/flask_vue
flask: https://gitee.com/l3210440292/my_flask_dev
blog： https://www.alfred-alan.online/ |
孙帅 | 将发送邮件和短信分别改造成异步任务定时任务-celery.log文件。 | 暂时没有 | vue:  https://gitee.com/sun_shuai_gitee/myvue.git
django: https://gitee.com/sun_shuai_gitee/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 | 完成邮箱和短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 |  https://gitee.com/yy20010514/my_flask.git
高伟鹏 | 完成短信密码找回，完成resutful风格接口 | 今天在启用celery时出现了redis连不上的错误,后来把配置文件中的locahost改成127.0.0.1,端口号不变,所以今天我应该加强一下端口和ip之间知识点整理 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |

day02 06-01

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue: https://gitee.com/l3210440292/my_vue_dev/tree/dev/
django:  https://gitee.com/l3210440292/my_django_dev/tree/dev/
blog： https://www.alfred-alan.online/ |
孙帅 | 将发送邮件和短信分别改造成异步任务定时任务-celery.log文件。 | 暂时没有 | vue:  https://gitee.com/sun_shuai_gitee/myvue.git
django: https://gitee.com/sun_shuai_gitee/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 | 完成邮箱和短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |https://gitee.com/yy20010514/my-django.git        https://gitee.com/yy20010514/my-vue.git     
高伟鹏 | 完成短信密码找回，完成resutful风格接口 | 今天在启用celery时出现了redis连不上的错误,后来把配置文件中的locahost改成127.0.0.1,端口号不变,所以今天我应该加强一下端口和ip之间知识点整理 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |

day02 05-29


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
孙帅 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue:  https://gitee.com/sun_shuai_gitee/myvue.git
django: https://gitee.com/sun_shuai_gitee/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |
高伟鹏 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |



day01 05-28


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
孙帅 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue:  https://gitee.com/sun_shuai_gitee/myvue.git
django: https://gitee.com/sun_shuai_gitee/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |
高伟鹏 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |


