day01 06-028


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 完成用户微服务系统 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
flask:https://gitee.com/song_yu_jie/myflask
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 完成celery | celery服务启动报错，更改启动方式celery worker -A mydjango -l info --pool=solo | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | flask | 暂时没有 |    flask：https://gitee.com/wang_xiao_rui/myflask_dev.git
https://ge_shu_ying.gitee.io/hugo_blog |
李德福 | 完成短信密码找回，完成celery异步发Email, | 使用老师的腾讯云sms未收到msg;阿里云sms注册ok,因收费停止验证；celery定时beat未响应；TODO:使用AsyncResult(id=task_id),未解决 `AttributeError: 'DisabledBackend' object has no attribute '_get_task_meta_for'`;换用redis,redis_cli.get(key)=None，exit(key)=None，换成get("不可变的str")能取到值，开发中止,浪费心情 | https://gitee.com/justblue/myvue_dev.git https://gitee.com/justblue/mydjango_master.git https://www.cnblogs.com/justblue/

https://ge_shu_ying.gitee.io/hugo_blog |
孙维妙 | 聊天 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/myflask.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |




day01 06-027


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 完成用户微服务系统 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
flask:https://gitee.com/song_yu_jie/myflask
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 完成celery | celery服务启动报错，更改启动方式celery worker -A mydjango -l info --pool=solo | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | flask | 暂时没有 |    flask：https://gitee.com/wang_xiao_rui/myflask_dev.git
https://ge_shu_ying.gitee.io/hugo_blog |
李德福 | 完成短信密码找回，完成celery异步发Email, | 使用老师的腾讯云sms未收到msg;阿里云sms注册ok,因收费停止验证；celery定时beat未响应；TODO:使用AsyncResult(id=task_id),未解决 `AttributeError: 'DisabledBackend' object has no attribute '_get_task_meta_for'`;换用redis,redis_cli.get(key)=None，exit(key)=None，换成get("不可变的str")能取到值，开发中止,浪费心情 | https://gitee.com/justblue/myvue_dev.git https://gitee.com/justblue/mydjango_master.git https://www.cnblogs.com/justblue/

https://ge_shu_ying.gitee.io/hugo_blog |
孙维妙 | 用户认证微服务系统 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/myflask.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |


day03 06-01


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 完成celery | celery异步执行任务时报错，通过升级redis解决 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 完成celery | celery服务启动报错，更改启动方式celery worker -A mydjango -l info --pool=solo | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://ge_shu_ying.gitee.io/hugo_blog |
李德福 | 完成短信密码找回，完成celery异步发Email, | 使用老师的腾讯云sms未收到msg;阿里云sms注册ok,因收费停止验证；celery定时beat未响应；TODO:使用AsyncResult(id=task_id),未解决 `AttributeError: 'DisabledBackend' object has no attribute '_get_task_meta_for'`;换用redis,redis_cli.get(key)=None，exit(key)=None，换成get("不可变的str")能取到值，开发中止,浪费心情 | https://gitee.com/justblue/myvue_dev.git https://gitee.com/justblue/mydjango_master.git https://www.cnblogs.com/justblue/

https://ge_shu_ying.gitee.io/hugo_blog |
孙维妙 | 完成邮件短信异步更改，完成利用定时任务每两小时写入一个celery.log文件 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |




day02 05-29


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://ge_shu_ying.gitee.io/hugo_blog |
李德福 | 完成短信密码找回，完成resutful风格接口 | 基本不会 | https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://ge_shu_ying.gitee.io/hugo_blog |
孙维妙 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |




day01 05-28


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | django：https://gitee.com/wang_xiao_rui/mydjango_dev.git
vue:https://gitee.com/wang_xiao_rui/myvue_dev.git
博客:https://wang_xiao_rui.gitee.io/wang_ruis_blog |
李德福 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
孙维妙 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |