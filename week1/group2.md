day02 06-28

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 在线客服 | 暂时没有 | https://gitee.com/i_xiao_luo/my_flask/tree/dev/ flask
https://gitee.com/i_xiao_luo/Vue2 vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 在线客服系统 | 暂时没有 | 5 |
王增辉 | 在线客服 | 没有 | flask地址：https://gitee.com/wang_zeng_hui/myflask  vue地址：https://gitee.com/wang_zeng_hui/myflask_vue
博客地址：https://okwzh.cn
王鑫 | socket全双工在线客服系统 | 暂时没有 | https://gitee.com/wxb_857/my_flask
https://gitee.com/wxb_857/big_myvue
https://wxb_857.gitee.io/blog/ |
聂明华 | 暂时没有 | https://gitee.com/nieminghua/mydjango
https://gitee.com/nieminghua/myvue
https://nieminghua.gitee.io/blog/ |


day02 06-27

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/i_xiao_luo/my_django django
https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 5 |
王增辉 | flask重构 | 没有 | flask地址：https://gitee.com/wang_zeng_hui/myflask
vue地址：
https://gitee.com/wang_zeng_hui/myflask_vue
博客地址：
https://okwzh.cn
王鑫 | flask重构用户操作 | 暂时没有 | https://gitee.com/wxb_857/my_flask
https://gitee.com/wxb_857/big_myvue
https://wxb_857.gitee.io/blog/ |
王涵 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wanghan123456/mydjango123
https://gitee.com/wanghan123456/myvue123
http://wanghan123456.gitee.io/my_technology_blog |
暂时没有 | https://gitee.com/nieminghua/mydjango
https://gitee.com/nieminghua/myvue
https://nieminghua.gitee.io/blog/ |




day02 06-01

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/i_xiao_luo/my_django django
https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 5 |
王增辉 | 了解celery原理，实现定时功能 | 问题写进博客了 | django地址：
https://gitee.com/wang_zeng_hui/mydjango_dev
vue地址：
https://gitee.com/wang_zeng_hui/myvue_dev
博客地址：
https://wang_zeng_hui.gitee.io/ |
王鑫 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 5 |
王涵 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wanghan123456/mydjango123
https://gitee.com/wanghan123456/myvue123
http://wanghan123456.gitee.io/my_technology_blog |
暂时没有 | https://gitee.com/nieminghua/mydjango
https://gitee.com/nieminghua/myvue
https://nieminghua.gitee.io/blog/ |


day02 05-29

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/i_xiao_luo/my_django django
https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 5 |
王增辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | django地址：
https://gitee.com/wang_zeng_hui/mydjango_dev
vue地址：
https://gitee.com/wang_zeng_hui/myvue_dev
博客地址：
https://wang_zeng_hui.gitee.io/ |
王鑫 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 5 |
王涵 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wanghan123456/mydjango123
https://gitee.com/wanghan123456/myvue123
http://wanghan123456.gitee.io/my_technology_blog |
暂时没有 | https://gitee.com/nieminghua/mydjango
https://gitee.com/nieminghua/myvue
https://nieminghua.gitee.io/blog/ |



day01 05-28


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成短信密码找回，完成resutful风格接口 | 没有 | https://gitee.com/i_xiao_luo/my_django django
https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
王增辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | django地址：
https://gitee.com/wang_zeng_hui/mydjango_dev
vue地址：
https://gitee.com/wang_zeng_hui/myvue_dev
博客地址：
https://wang_zeng_hui.gitee.io/ |
王鑫 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
王涵 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wanghan123456/mydjango123
https://gitee.com/wanghan123456/myvue123
http://wanghan123456.gitee.io/my_technology_blog |
聂明华 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/nieminghua/mydjango
https://gitee.com/nieminghua/myvue
https://nieminghua.gitee.io/blog/ |