day19 06-22

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
蒋枨文 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 使用原生sql查询全部标签，对标签系统的构造 | 暂时没有 | vue:https://gitee.com/wangxs020202/edu_vue/branches django:https://gitee.com/wangxs020202/edu_dj/branches 博客：https://www.sirxs.cn/ |
田文艳 | 钉钉机器人推送天气 | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/xiaotian18/mydjango1.git |
郭其鑫 |使用原生sql查询全部标签， | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 钉钉机器人推送天气 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 使用celery钉钉机器人定时推送天气 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |


day18 06-19

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
蒋枨文 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | d18作业 | 暂时没有 | vue:https://gitee.com/wangxs020202/edu_vue/branches django:https://gitee.com/wangxs020202/edu_dj/branches 博客：https://www.sirxs.cn/ |
田文艳 | 钉钉机器人推送天气 | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/xiaotian18/mydjango1.git |
郭其鑫 |钉钉机器人推送天气 | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 钉钉机器人推送天气 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 使用celery钉钉机器人定时推送天气 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |


day17 06-18

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
蒋枨文 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 完成支付宝退款 | 暂时没有 | vue:https://gitee.com/wangxs020202/edu_vue/branches django:https://gitee.com/wangxs020202/edu_dj/branches 博客：https://www.sirxs.cn/ |
田文艳 | 钉钉机器人推送天气 | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/xiaotian18/mydjango1.git |
郭其鑫 | 退款功能 | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 完成支付宝退款、完善单例模式 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 完成支付宝退款 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |


day16 06-17

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
蒋枨文 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 完成PayPal支付 | 暂时没有 | vue:https://gitee.com/wangxs020202/edu_vue/branches django:https://gitee.com/wangxs020202/edu_dj/branches 博客：https://www.sirxs.cn/ |
田文艳 | paypal支付 | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/tian_wen_yan/mydjango1.git |
郭其鑫 | 完成跨域支付 改造工厂模式 | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 完成paypal支付 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 完成paypal支付 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |

day15 06-16

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
蒋枨文 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 完成支付宝支付 | 暂时没有 | vue:https://gitee.com/wangxs020202/edu_vue/branches django:https://gitee.com/wangxs020202/edu_dj/branches 博客：https://www.sirxs.cn/ |
田文艳 | 支付包支付 | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/tian_wen_yan/mydjango1.git |
郭其鑫 | 完成支付宝支付 | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 完成支付宝支付| 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 完成支付宝支付 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |

day14 06-15

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
蒋枨文 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 完成订单接口加签 | 暂时没有 | vue:https://gitee.com/wangxs020202/edu_vue/branches django:https://gitee.com/wangxs020202/edu_dj/branches 博客：https://www.sirxs.cn/ |
田文艳 | 订单验证，完成resutful风格接口 | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/tian_wen_yan/mydjango1.git |
郭其鑫 | 完成订单验证 | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 完成订单加签 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 完成订单验证签名 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |


day15 06-16


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
蒋枨文 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue:https://gitee.com/wangxs020202/edu_vue/commits/develop                                                      django:https://gitee.com/wangxs020202/edu_dj/tree/develop/                                                                  博客：https://www.sirxs.cn/ |
田文艳 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/tian_wen_yan/mydjango1.git |
郭其鑫 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 完成支付宝付款功能 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |



day01 06-15

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
蒋枨文 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/jiang_wen_wen/vue_dev
https://gitee.com/jiang_wen_wen/django_dev
https://blog.csdn.net/weixin_45580706 |
王晓生 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | vue:https://gitee.com/wangxs020202/edu_vue/commits/develop                                                      django:https://gitee.com/wangxs020202/edu_dj/tree/develop/                                                                  博客：https://www.sirxs.cn/ |
田文艳 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://tian_wen_yan.gitee.io/hexo/
https://gitee.com/tian_wen_yan/myvue1.git
https://gitee.com/tian_wen_yan/mydjango1.git |
郭其鑫 | 完成订单验证 | 暂时没有 | Vue:  https://gitee.com/guo_qi_xin/Gmyvue/tree/dev
Django：https://gitee.com/guo_qi_xin/Gmydjango/tree/dev/
博客： http://gqx.9gz.xyz/ |
范佳乐 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://fjlxiaozhuanjia.gitee.io/hugoblog
https://gitee.com/fjlxiaozhuanjia/myvue_dev/tree/dev
https://gitee.com/fjlxiaozhuanjia/mydjango_dev/tree/dev |
王超 | 完成订单验证签名 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |