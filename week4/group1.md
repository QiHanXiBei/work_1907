
day07 06-23

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成抽奖系统 | 暂时没有 | https://gitee.com/asyx/zhouce |
刘吉庆 |使用原生sql查询全部标签，对标签系统的构造 | 暂时没有 | vue: https://gitee.com/l3210440292/my_vue_dev/tree/dev/
django:  https://gitee.com/l3210440292/my_django_dev/tree/dev/
blog：https://www.alfred-alan.online/ |
孙帅 |标签sql原始语句的结合| 暂时没有 | vue:  https://gitee.com/s_s_h/myvue.git
django: https://gitee.com/s_s_h/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 |通过原生语句完成对标签的相关查询| 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 完成paypal支付 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |  
https://gitee.com/yy20010514/my-django.git  |  https://gitee.com/yy20010514/my-vue.git
高伟鹏 | 原生sql进行多表关联查询标签 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |**


day07 06-22

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成原生sql进行多表关联查询 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 |使用原生sql查询全部标签，对标签系统的构造 | 暂时没有 | vue: https://gitee.com/l3210440292/my_vue_dev/tree/dev/
django:  https://gitee.com/l3210440292/my_django_dev/tree/dev/
blog：https://www.alfred-alan.online/ |
孙帅 |标签sql原始语句的结合| 暂时没有 | vue:  https://gitee.com/s_s_h/myvue.git
django: https://gitee.com/s_s_h/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 |通过原生语句完成对标签的相关查询| 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 完成paypal支付 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |  
https://gitee.com/yy20010514/my-django.git  |  https://gitee.com/yy20010514/my-vue.git
高伟鹏 | 原生sql进行多表关联查询标签 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |**


day07 06-21

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成钉钉机器人退款提示和事务性 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 |完成钉钉机器人推送消息，定时天气推送 | 暂时没有 | vue: https://gitee.com/l3210440292/my_vue_dev/tree/dev/
django:  https://gitee.com/l3210440292/my_django_dev/tree/dev/
blog：https://www.alfred-alan.online/ |
孙帅 |退款成功钉钉主动推动信息， 发送天气预报| 暂时没有 | vue:  https://gitee.com/s_s_h/myvue.git
django: https://gitee.com/s_s_h/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 |	完成钉钉机器人推送消息，定时天气推送 | 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 完成paypal支付 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |  
https://gitee.com/yy20010514/my-django.git  |  https://gitee.com/yy20010514/my-vue.git
高伟鹏 | 退款事务逻辑和推送天气 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |**

day05 06-19


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成退款和查询是否付款操作 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 完成退款服务，完成同步支付状态 | 暂时没有 | vue: https://gitee.com/l3210440292/my_vue_dev/tree/dev/
django:  https://gitee.com/l3210440292/my_django_dev/tree/dev/
blog：https://www.alfred-alan.online/ |
孙帅 |后续退款服务，接口加入订单状态同步功能 | 暂时没有 | vue:  https://gitee.com/s_s_h/myvue.git
django: https://gitee.com/s_s_h/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 |	完成退款钉钉消息通知 | 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 完成paypal支付 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |  
https://gitee.com/yy20010514/my-django.git  |  https://gitee.com/yy20010514/my-vue.git
高伟鹏 | 完成退款和同步支付 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |**



day04 06-18


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成退款和查询是否付款操作 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 完成退款服务，完成同步支付状态 | 暂时没有 | vue: https://gitee.com/l3210440292/my_vue_dev/tree/dev/
django:  https://gitee.com/l3210440292/my_django_dev/tree/dev/
blog：https://www.alfred-alan.online/ |
孙帅 |后续退款服务，接口加入订单状态同步功能 | 暂时没有 | vue:  https://gitee.com/sun_shuai_gitee/myvue.git
django: https://gitee.com/sun_shuai_gitee/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 |	完成退款服务| 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 完成paypal支付 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |  
https://gitee.com/yy20010514/my-django.git  |  https://gitee.com/yy20010514/my-vue.git
高伟鹏 | 完成退款和同步支付 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |**

day03 06-17


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成paypal支付 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 完成paypal第三方支付 | 暂时没有 | vue: https://gitee.com/l3210440292/my_vue_dev/tree/dev/
django:  https://gitee.com/l3210440292/my_django_dev/tree/dev/
blog：https://www.alfred-alan.online/ |
孙帅 | 完成海外paypal第三方支付 | 暂时没有 | vue:  https://gitee.com/sun_shuai_gitee/myvue.git
django: https://gitee.com/sun_shuai_gitee/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 |	完成paypal支付 | 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 完成paypal支付 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |  
https://gitee.com/yy20010514/my-django.git  |  https://gitee.com/yy20010514/my-vue.git
高伟鹏 | 完成paypal跨境支付 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |** 

 day02 06-16


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成支付宝第三方支付 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 完成支付宝第三方支付 | 暂时没有 | vue: https://gitee.com/l3210440292/my_vue_dev/tree/dev/
django:  https://gitee.com/l3210440292/my_django_dev/tree/dev/
blog：https://www.alfred-alan.online/ |
孙帅 | 完成支付宝第三方支付 | 暂时没有 | vue:  https://gitee.com/sun_shuai_gitee/myvue.git
django: https://gitee.com/sun_shuai_gitee/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 |完成支付宝第三方支付 | 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 完成支付宝支付 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |  
https://gitee.com/yy20010514/my-django.git  |  https://gitee.com/yy20010514/my-vue.git
高伟鹏 | 支付宝支付集成和付款 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |** 

day01 06-15


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
史愉翔 | 完成订单签名加密 | 暂时没有 | vue: https://gitee.com/asyx/Fvue
django:  https://gitee.com/asyx/Fdjango
blog： https://asyx.gitee.io/bike |
刘吉庆 | 完成支付接口加密签名 | 暂时没有 | vue: https://gitee.com/l3210440292/my_vue_dev/tree/dev/
django:  https://gitee.com/l3210440292/my_django_dev/tree/dev/
blog：https://www.alfred-alan.online/ |
孙帅 | 订单接口加签名验证 | 暂时没有 | vue:  https://gitee.com/sun_shuai_gitee/myvue.git
django: https://gitee.com/sun_shuai_gitee/mydjango.git
blog：http://sun_shuai_gitee.gitee.io/simonblog/ |
梁野 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 作业项目地址 笔记：https://www.cnblogs.com/langye521erxia/ |
齐书舵 | 完成订单签名加密 | 暂时没有 | https://gitee.com/qishuduo/mydjango_dev/branches
https://gitee.com/qishuduo/myvue_fenzhi/branches
博客http://qishuduo.gitee.io/my_technology_blog/ |
元鹏杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 |  https://gitee.com/yy20010514/blog.git |
高伟鹏 | 标签加密和完善订单 | 暂时没有 | https://gitee.com/gao_wei_peng/mydjango_dev django https://gitee.com/gao_wei_peng/myvue_dev vue https://gao_wei_peng.gitee.io/hugoblog 博客  |



