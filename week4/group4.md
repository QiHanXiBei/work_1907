
day07 06-23


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 完成考试,所有文件都在Django项目里 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog
Django:https://gitee.com/song_yu_jie/django_test
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 完成celery | celery服务启动报错，更改启动方式celery worker -A mydjango -l info --pool=solo | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 企业级抽奖 | 暂时没有| django：https://gitee.com/wang_xiao_rui/mydjango_dev.git
vue:https://gitee.com/wang_xiao_rui/myvue_dev.git
博客:https://wang_xiao_rui.gitee.io/wang_ruis_blog |
李德福 | 完成推送DingTalk | 暂时没有| https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://hear2ken.cn |
孙维妙 | 完成原生sql查询课程标签 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |


day07 06-22


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 完成退款事务逻辑,退款结合钉钉机器人通知消息,钉钉机器人定时发送天气情况 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 完成celery | celery服务启动报错，更改启动方式celery worker -A mydjango -l info --pool=solo | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 推荐原生SQL查询 | 暂时没有| django：https://gitee.com/wang_xiao_rui/mydjango_dev.git
vue:https://gitee.com/wang_xiao_rui/myvue_dev.git
博客:https://wang_xiao_rui.gitee.io/wang_ruis_blog |
李德福 | 完成推送DingTalk | 暂时没有| https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://hear2ken.cn |
孙维妙 | 完成原生sql查询课程标签 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |

day07 06-21


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 完成退款事务逻辑,退款结合钉钉机器人通知消息,钉钉机器人定时发送天气情况 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 完成celery | celery服务启动报错，更改启动方式celery worker -A mydjango -l info --pool=solo | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 退款事务逻辑 推送天气 推送退款是否成功 | 暂时没有| django：https://gitee.com/wang_xiao_rui/mydjango_dev.git
vue:https://gitee.com/wang_xiao_rui/myvue_dev.git
博客:https://wang_xiao_rui.gitee.io/wang_ruis_blog |
李德福 | 完成推送DingTalk | 暂时没有| https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://hear2ken.cn |
孙维妙 | 完成推送信息(钉钉机器人)到钉钉群内 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |


day07 06-18


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 完成退款功能 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 完成celery | celery服务启动报错，更改启动方式celery worker -A mydjango -l info --pool=solo | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 退款 | 暂时没有| django：https://gitee.com/wang_xiao_rui/mydjango_dev.git
vue:https://gitee.com/wang_xiao_rui/myvue_dev.git
博客:https://wang_xiao_rui.gitee.io/wang_ruis_blog |
李德福 | 完成支付宝退款 | 已解决，贝宝payerID验证失败 | https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://hear2ken.cn |
孙维妙 | 完成退款 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |

day06 06-17


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 完成paypal跨境支付 | paypal接口在国外,请求接口时特别慢,可以考虑安装谷歌助手,会加快速度 | 博客:https://song_yu_jie.gitee.io/hugoblog
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 完成celery | celery服务启动报错，更改启动方式celery worker -A mydjango -l info --pool=solo | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | paypal跨境支付，将支付宝和paypal集成工厂模式 | 关于paypal流水id打印为空的原因 在验证支付者id之前打印流水id是打印不到的，只有进行判断后支付成功打印流水id不会为空，解决任何将鼠标放在图片上变为手的操作 在style中设置 cursor: pointer 就可以解决 | django：https://gitee.com/wang_xiao_rui/mydjango_dev.git
vue:https://gitee.com/wang_xiao_rui/myvue_dev.git
博客:https://wang_xiao_rui.gitee.io/wang_ruis_blog |
李德福 | 完成paypal支付 | 支付复杂业务，无法使用restful风格 | https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://hear2ken.cn |
孙维妙 | 完成paypal跨境支付 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |

day05 06-16


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 完成沙箱应用结合购买课程 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 完成celery | celery服务启动报错，更改启动方式celery worker -A mydjango -l info --pool=solo | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 支付宝沙箱 | 暂时没有 | django：https://gitee.com/wang_xiao_rui/mydjango_dev.git
vue:https://gitee.com/wang_xiao_rui/myvue_dev.git
博客:https://wang_xiao_rui.gitee.io/wang_ruis_blog |
李德福 | 完成支付宝sandbox支付 | 由于django设置了simplejwt身份认证，但是支付回调请求header没法携带bearer token, 就很尴尬了，回调view类关闭身份认证安全性如何 | https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://hear2ken.cn |
孙维妙 | 完成支付宝付款功能 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |


day04 06-15


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 完成加密订单信息 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 完成celery | celery服务启动报错，更改启动方式celery worker -A mydjango -l info --pool=solo | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://ge_shu_ying.gitee.io/hugo_blog |
李德福 | 完成订单md5加密安全传输 | 暂时没有 | https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://hear2ken.cn |
孙维妙 | 完成订单的接口加签（验证签名） | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |


day02 05-29


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/ge_shu_ying/mydjango_dev.git    
https://gitee.com/ge_shu_ying/myvue_dev.git
https://ge_shu_ying.gitee.io/hugo_blog |
李德福 | 完成短信密码找回，完成celery异步发Email, | hahahha | https://gitee.com/justblue/myvue_dev.git    
https://gitee.com/justblue/mydjango_master.git
https://ge_shu_ying.gitee.io/hugo_blog |
孙惟妙 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |




day01 05-28


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
宋玉杰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客:https://song_yu_jie.gitee.io/hugoblog/
Django:https://gitee.com/song_yu_jie/mydjango_dev
vue:https://gitee.com/song_yu_jie/myvue_dev |
张易洺 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/ZhangYiMing010208/myvue_dev.git
https://gitee.com/ZhangYiMing010208/mydjango_dev.git http://zhangyiming010208.gitee.io/hugo  |
陈登辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 博客：https://mp.csdn.net/console/article
vue:https://gitee.com/jekky/myvue_dev/tree/dev
django:https://gitee.com/jekky/mydjango_dev/tree/dev |
王瑞 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | django：https://gitee.com/wang_xiao_rui/mydjango_dev.git
vue:https://gitee.com/wang_xiao_rui/myvue_dev.git
博客:https://wang_xiao_rui.gitee.io/wang_ruis_blog |
李德福 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
孙惟妙 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/sun_wei_miao/myvue    https://gitee.com/sun_wei_miao/mydjango.git  https://sun_wei_miao.gitee.io/my_technology_blog/ |