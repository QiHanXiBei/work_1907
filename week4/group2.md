day04 06-122

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成原生sql操作| 暂时没有 | https://gitee.com/i_xiao_luo/my_django django<br>https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成原生sql操作  | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev<br>https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
王增辉 | 完成标签功能 | 没有 | django地址：https://gitee.com/wang_zeng_hui/mydjango_dev<br>vue地址：https://gitee.com/wang_zeng_hui/myvue_dev
https://wang_zeng_hui.gitee.io/ |
王鑫 | 使用原生sql实现对数据库操作| 没有 | https://gitee.com/wxb_857/big_mydjango <br> https://gitee.com/wxb_857/big_myvue
 https://wxb_857.gitee.io/blog/ |
王涵 | 完成分布式订单存储 | 暂时没有 | https://gitee.com/wanghan123456/mydjango_dev<br>https://gitee.com/wanghan123456/myvue_dev
https://wanghan123-github.github.io/ |
聂明华 | 标签功能 | 暂无 | https://gitee.com/nieminghua/mydjango<br>https://gitee.com/nieminghua/myvue<br>https://nieminghua.gitee.io/blog/ |** 





day03 06-18

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成钉钉推送| 暂时没有 | https://gitee.com/i_xiao_luo/my_django django<br>https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成钉钉推送  | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev<br>https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
王增辉 | 完成钉钉机器人定时推送天气预报 | 没有 | django地址：https://gitee.com/wang_zeng_hui/mydjango_dev<br>vue地址：https://gitee.com/wang_zeng_hui/myvue_dev
https://wang_zeng_hui.gitee.io/ |
王鑫 | 实现钉钉定时推送 | 没有 | https://gitee.com/wxb_857/big_mydjango <br> https://gitee.com/wxb_857/big_myvue
 https://wxb_857.gitee.io/blog/ |
王涵 | 完成分布式订单存储 | 暂时没有 | https://gitee.com/wanghan123456/mydjango_dev<br>https://gitee.com/wanghan123456/myvue_dev
https://wanghan123-github.github.io/ |
聂明华 | 钉钉推送 | 暂无 | https://gitee.com/nieminghua/mydjango<br>https://gitee.com/nieminghua/myvue<br>https://nieminghua.gitee.io/blog/ |** 



day03 06-17

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成支付| 暂时没有 | https://gitee.com/i_xiao_luo/my_django django<br>https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成支付  | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev<br>https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
王增辉 | 实现paypal境外支付 | 没有 | django地址：https://gitee.com/wang_zeng_hui/mydjango_dev<br>vue地址：https://gitee.com/wang_zeng_hui/myvue_dev
https://wang_zeng_hui.gitee.io/ |
王鑫 | 工厂模式实现paypal支付接口 | 没有 | https://gitee.com/wxb_857/big_mydjango <br> https://gitee.com/wxb_857/big_myvue
 https://wxb_857.gitee.io/blog/ |
王涵 | 完成分布式订单存储 | 暂时没有 | https://gitee.com/wanghan123456/mydjango_dev<br>https://gitee.com/wanghan123456/myvue_dev
https://wanghan123-github.github.io/ |
聂明华 | 支付 | 暂无 | https://gitee.com/nieminghua/mydjango<br>https://gitee.com/nieminghua/myvue<br>https://nieminghua.gitee.io/blog/ |** 






day02 06-16

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成支付| 暂时没有 | https://gitee.com/i_xiao_luo/my_django django<br>https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成支付  | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev<br>https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
王增辉 | 完成支付功能 | 没有 | django地址：https://gitee.com/wang_zeng_hui/mydjango_dev<br>vue地址：https://gitee.com/wang_zeng_hui/myvue_dev
https://wang_zeng_hui.gitee.io/ |
王鑫 | 完成支付宝支付接口| 没有 | https://gitee.com/wxb_857/big_mydjango<br>https://gitee.com/wxb_857/big_myvue
https://wxb_857.gitee.io/blog/ |
王涵 | 完成分布式订单存储 | 暂时没有 | https://gitee.com/wanghan123456/mydjango_dev<br>https://gitee.com/wanghan123456/myvue_dev
https://wanghan123-github.github.io/ |
聂明华 | 支付 | 暂无 | https://gitee.com/nieminghua/mydjango<br>https://gitee.com/nieminghua/myvue<br>https://nieminghua.gitee.io/blog/ |** 








day02 06-15

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成订单token验证| 暂时没有 | https://gitee.com/i_xiao_luo/my_django django<br>https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成订单token验证  | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev<br>https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
王增辉 | 完成分布式订单 | 没有 | django地址：https://gitee.com/wang_zeng_hui/mydjango_dev<br>vue地址：https://gitee.com/wang_zeng_hui/myvue_dev
https://wang_zeng_hui.gitee.io/ |
王鑫 | 标签加密以及完善订单查询| 没有 | https://gitee.com/wxb_857/big_mydjango<br>https://gitee.com/wxb_857/big_myvue
https://wxb_857.gitee.io/blog/ |
王涵 | 完成分布式订单存储 | 暂时没有 | https://gitee.com/wanghan123456/mydjango_dev<br>https://gitee.com/wanghan123456/myvue_dev
https://wanghan123-github.github.io/ |
聂明华 | 完成分布式订单存储 | 暂无 | https://gitee.com/nieminghua/mydjango<br>https://gitee.com/nieminghua/myvue<br>https://nieminghua.gitee.io/blog/ |** 


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/i_xiao_luo/my_django django
https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 5 |
王增辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | django地址：
https://gitee.com/wang_zeng_hui/mydjango_dev
vue地址：
https://gitee.com/wang_zeng_hui/myvue_dev
博客地址：
https://wang_zeng_hui.gitee.io/ |
王鑫 | 标签加密以及完善订单查询 | 暂时没有 | https://gitee.com/wxb_857/big_myvue 
 https://gitee.com/wxb_857/big_mydjango  https://wxb_857.gitee.io/blog|
王涵 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wanghan123456/mydjango123
https://gitee.com/wanghan123456/myvue123
http://wanghan123456.gitee.io/my_technology_blog |
暂时没有 | https://gitee.com/nieminghua/mydjango
https://gitee.com/nieminghua/myvue
https://nieminghua.gitee.io/blog/ |








day02 05-29

姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/i_xiao_luo/my_django django
https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 5 |
王增辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | django地址：
https://gitee.com/wang_zeng_hui/mydjango_dev
vue地址：
https://gitee.com/wang_zeng_hui/myvue_dev
博客地址：
https://wang_zeng_hui.gitee.io/ |
王鑫 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | 5 |
王涵 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wanghan123456/mydjango123
https://gitee.com/wanghan123456/myvue123
http://wanghan123456.gitee.io/my_technology_blog |
暂时没有 | https://gitee.com/nieminghua/mydjango
https://gitee.com/nieminghua/myvue
https://nieminghua.gitee.io/blog/ |



day01 05-28


姓名 | 作业 |  遇到的问题 | 每日工作内容和笔记  
-|-|-|-
罗峰 | 完成短信密码找回，完成resutful风格接口 | 没有 | https://gitee.com/i_xiao_luo/my_django django
https://gitee.com/i_xiao_luo/my_vue vue
https://i_xiao_luo.gitee.io/myhexo/ 博客 |
葛书盈 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
王增辉 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | django地址：
https://gitee.com/wang_zeng_hui/mydjango_dev
vue地址：
https://gitee.com/wang_zeng_hui/myvue_dev
博客地址：
https://wang_zeng_hui.gitee.io/ |
王鑫 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wang_chaoo/myvue_dev
https://gitee.com/wang_chaoo/mydjango_dev
http://wang_chaoo.gitee.io/my_technology_blog/ |
王涵 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/wanghan123456/mydjango123
https://gitee.com/wanghan123456/myvue123
http://wanghan123456.gitee.io/my_technology_blog |
聂明华 | 完成短信密码找回，完成resutful风格接口 | 暂时没有 | https://gitee.com/nieminghua/mydjango
https://gitee.com/nieminghua/myvue
https://nieminghua.gitee.io/blog/ |